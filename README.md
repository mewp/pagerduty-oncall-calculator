Usage:

    pd-oncall --help

Or:

    nix run -- --help

Or, without even cloning the repo:

    nix run gitlab:Mewp/pagerduty-oncall-calculator -- --help
